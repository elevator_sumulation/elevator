'use strict';

const _ = require('lodash');
const machina = require('machina');

const AWAITING_TIME = 2000;
const DEFAULT_FLOOR = 1;

const StateMachine = machina.Fsm.extend({

    initialState: 'chilling',

    initialize: function () {
        this.currentFloor = DEFAULT_FLOOR;

        // TODO: Get rid of these here
        this.innerButtons = new Set();
        this.outerButtons = new Set();
    },

    // Call on button click. Pass an array.
    innerButton: function (button) {
        this.innerButtons.add(button);
        this.handle('button');
    },

    outerButton: function (button) {
        this.outerButtons.add(button);
        this.handle('button');
    },

    // Call on a floor arrival. Parameters ain't needed.
    floor: function (floor) {
        this.currentFloor = floor;
        this.handle('floor');
    },

    doorOpened: function () {
        this.handle('doorOpened');
    },

    doorClosed: function () {
        this.handle('doorClosed');
    },

    canChill: function () {
        return this.currentFloor === DEFAULT_FLOOR && !this.innerButtons.size && !this.outerButtons.size;
    },

    getNextFloorStopped: function () {
        let nextFloor;

        const inner = _.toArray(this.innerButtons);
        const outer = _.toArray(this.outerButtons);

        if (inner.length) {
            const higherFloors = inner.filter((floor) => {
                return floor - this.currentFloor > 0;
            });

            if (higherFloors.length) {
                nextFloor = _.min(higherFloors);
            } else {
                const lowerFloors = inner.concat(outer).filter((floor) => {
                    return floor - this.currentFloor < 0;
                });
                nextFloor = _.max(lowerFloors);
            }
        } else if (outer.length) {
            nextFloor = _.max(outer);
        } else {
            nextFloor = DEFAULT_FLOOR;
        }

        return nextFloor;
    },

    getNextFloorMovingUp: function () {
        let nextFloor;

        const inner = _.toArray(this.innerButtons);
        const outer = _.toArray(this.outerButtons);

        if (inner.length) {
            const higherFloors = inner.filter((floor) => {
                return floor - this.currentFloor > 0;
            });

            if (higherFloors.length) {
                nextFloor = _.min(higherFloors);
            } else {
                nextFloor = this.nextFloor;
            }
        } else if (outer.length) {
            nextFloor = _.max(outer);
        } else {
            nextFloor = DEFAULT_FLOOR;
        }

        return nextFloor;
    },

    getNextFloorMovingDown: function () {
        let nextFloor;

        const inner = _.toArray(this.innerButtons);
        const outer = _.toArray(this.outerButtons);

        if (inner.length || outer.length) {
            const lowerFloors = inner.concat(outer).filter((floor) => {
                return floor - this.currentFloor < 0;
            });
            nextFloor = _.max(lowerFloors);
        } else {
            nextFloor = DEFAULT_FLOOR;
        }

        return nextFloor;
    },

    setNextFloor: function () {
        if (this.up) {
            this.nextFloor = this.getNextFloorMovingUp();
        } else if (this.down) {
            this.nextFloor = this.getNextFloorMovingDown();
        } else {
            this.nextFloor = this.getNextFloorStopped();
        }

        if (this.nextFloor > this.currentFloor) {
            this.up = true;
        } else if (this.nextFloor < this.currentFloor) {
            this.down = true;
        }
    },

    goToNextFloor: function () {
        if (this.up) {
            this.transition('movingUp');
        } else if (this.down) {
            this.transition('movingDown');
        }
    },

    states: {
        chilling: {
            _onEnter: function () {
            },

            button: function () {
                this.setNextFloor();
                this.handle('closeDoor');
            },

            closeDoor: function () {
                this.transition('doorClosing');
            }
        },

        waiting: {
            _onEnter: function () {
                this.timeout = setTimeout(() => {
                    this.handle('move');
                }, AWAITING_TIME);
            },

            move: function () {
                this.setNextFloor();
                this.handle('closeDoor');
            },

            closeDoor: 'doorClosing'
        },

        arrived: {
            _onEnter: function () {
                this.innerButtons.delete(this.currentFloor);
                this.outerButtons.delete(this.currentFloor);

                this.up = this.down = false;

                this.emit('stopped');

                this.handle('openDoor');
            },

            openDoor: 'doorOpening'
        },

        doorOpening: {
            _onEnter: function () {
                this.emit('door-opening');
            },

            doorOpened: function () {
                this.emit('door-opened');

                if (this.canChill()) {
                    this.transition('chilling');
                } else {
                    this.transition('waiting');
                }
            }
        },

        doorClosing: {
            _onEnter: function () {
                this.emit('door-closing');
            },

            doorClosed: function () {
                this.emit('door-closed');

                this.goToNextFloor();
            }
        },

        movingUp: {
            _onEnter: function () {
                this.emit('moving-up');
            },

            button: function () {
                this.setNextFloor();
                this.goToNextFloor();
            },

            floor: function () {
                if (this.currentFloor === this.nextFloor) {
                    this.transition('arrived');
                }
            }
        },

        movingDown: {
            _onEnter: function () {
                this.emit('moving-down');
            },

            button: function () {
                this.setNextFloor();
                this.goToNextFloor();
            },

            floor: function ()  {
                if (this.currentFloor === this.nextFloor) {
                    this.transition('arrived');
                }
            }
        }
    }
});

module.exports = StateMachine;
