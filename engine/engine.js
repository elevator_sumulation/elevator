'use strict';

const config = require('../config/elevator_config.json');
const StateMachine = require('./stateMachine');

function Engine () {

    const stateMachine = this.stateMachine = new StateMachine();

    let timeout = null;
    let velocity = 0;
    let doorVelocity = 0;

    this.started = false;
    this.position = 0;
    this.doorPosition = -config.doorWidth;

    const updateFloor = () => {
        if ((this.position % config.floorHeight === 0 || this.position === 0)) {
            const currentFloor = Math.floor(this.position / config.floorHeight) + 1;
            stateMachine.floor(currentFloor);
        }
    }

    const checkDoorPosition = () => {
        if (doorVelocity) {
            if (this.doorPosition <= -config.doorWidth) {
                this.doorPosition = -config.doorWidth;
                stateMachine.doorOpened();
            } else if (this.doorPosition >= 0) {
                this.doorPosition = 0;
                stateMachine.doorClosed();
            }
        }
    }

    stateMachine.on('moving-up', () => {
        velocity = config.velocity;
    });

    stateMachine.on('moving-down', () => {
        velocity = -config.velocity;
    });

    stateMachine.on('stopped', () => {
        velocity = 0;
    });

    stateMachine.on('door-opening', () => {
        doorVelocity = -config.doorVelocity;
    });

    stateMachine.on('door-closing', () => {
        doorVelocity = config.doorVelocity;
    });

    stateMachine.on('door-opened', () => {
        doorVelocity = 0;
    });

    stateMachine.on('door-closed', () => {
        doorVelocity = 0;
    })

    this.start = () => {
        this.started = true;

        var tick = () => {
            if (this.started) {

                this.position += velocity;
                this.doorPosition += doorVelocity;

                updateFloor();
                checkDoorPosition();

                this.timeout = setTimeout(tick, config.tickInterval);
            }
        }

        tick();
    }

    this.stop = () => {
        this.started = false;
        clearTimeout(this.timeout);
        this.timeout = null;
    }

    this.onInnerClicked = (button) => {
        stateMachine.innerButton(button);
    }

    this.onOuterClicked = (button) => {
        stateMachine.outerButton(button);
    }
}

module.exports = Engine;
