"use strict";
var socket = io('http://localhost:3000');
var countStage = 0;
var state = {
    state: 'elevator',
    y: 0,
    x: 0,
    currentStage: 1,
    nextStage: 1,
    innerSet: [],
    outerSet: []
};

function isNumeric(value) {
    return /^\d+$/.test(value);
}
socket.on('get_countStage', function(data) {
    console.log(countStage);
    console.log(data);
    if (0 === data) {
        countStage = prompt('сколько этажей?', 10);
        let isnumber = isNumeric(countStage);
        let moreThanZero = parseInt(countStage, 10);
        while (!isnumber || (moreThanZero <= 0)) {
            countStage = prompt('нужно ввести число! сколько этажей?', 10);
            isnumber = isNumeric(countStage);
            moreThanZero = parseInt(countStage, 10);
        }
        socket.emit('set_countStage', countStage);
    } else {
        countStage = data;
    }
    init();
    animate();
});

socket.on('state', function(data) {
    state = data;
})

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var INTERSECTED;

var lastinnerSet, lastOuterSet;
var innerSet = new Set();
var outerSet = new Set();

var container;
var camera, controls, scene, renderer, light, stats;
var arrayStage = [],
    arrayDoor = [],
    arrayStageBox = [],
    arrayTextNumber = [];
var elevatorDoor, elevator;
var dataController = {
    selectInnerStage: 1,
    selectOuterStage: 1,
    sendInnerStage: function() {
        socket.emit("select_inner_stage", this.selectInnerStage);
    },
    sendOuterStage: function() {
        socket.emit("select_outer_stage", this.selectOuterStage);
    },
    rentgen: false
};


function init() {
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000 * countStage);
    camera.position.z = 500;
    camera.position.y = 60 * countStage;

    scene = new THREE.Scene();
    scene.fog = new THREE.FogExp2(0xbfd1e5, 0.00005);

    light = new THREE.DirectionalLight();
    light.position.set(1, 1, 1);
    scene.add(light);

    light = new THREE.DirectionalLight();
    light.position.set(-1, 1, -1);
    scene.add(light);

    var gt = new THREE.TextureLoader().load("grasslight-big.jpg");
    var gg = new THREE.PlaneBufferGeometry(1600, 1600);
    var gm = new THREE.MeshPhongMaterial({
        color: 0xffffff,
        map: gt
    });
    var ground = new THREE.Mesh(gg, gm);
    ground.rotation.x = -Math.PI / 2;
    ground.material.map.repeat.set(64, 64);
    ground.material.map.wrapS = THREE.RepeatWrapping;
    ground.material.map.wrapT = THREE.RepeatWrapping;
    scene.add(ground);

    Promise.all([loadModelOfObjMtl("stage"), loadModelOfObjMtl("door"), loadModelOfObjMtl("roof"), loadFont("optimer_regular.typeface.js")]).then(results => {
        let modelStage = results[0];
        let modelDoor = results[1];
        let modelRoof = results[2];
        let font = results[3];

        let y = 0;

        for (let i = 0; i < countStage; i++) {
            y = 30 * i;
            modelStage.position.y = y;
            let cloneModelStage = modelStage.clone();
            arrayStage.push(cloneModelStage.children);
            scene.add(cloneModelStage);

            modelDoor.position.y = y;
            let cloneModelDoor = modelDoor.clone();
            arrayDoor.push(cloneModelDoor);
            scene.add(cloneModelDoor);

            let textGeo = new THREE.TextGeometry(String(i + 1), {
                font: font,
                size: 28,
                height: 5,
            });

            let textMesh = new THREE.Mesh(textGeo, new THREE.MeshBasicMaterial({
                color: 0xabdf17
            }));
            textMesh.name = String(i + 1);

            textMesh.position.x = -45;
            textMesh.position.z = 80;
            textMesh.position.y = 30 * i + 1;

            arrayTextNumber.push(textMesh);
            scene.add(textMesh);

            let stageBox = new THREE.BoxGeometry(15.2, 9.2, 3);
            let stageBoxMesh = new THREE.Mesh(stageBox, new THREE.MeshBasicMaterial({
                color: 0xffffff,
                depthWrite: false,
                transparent: true,
                opacity: 0
            }));

            stageBoxMesh.scale.x = stageBoxMesh.scale.y = stageBoxMesh.scale.z = 10;
            stageBoxMesh.rotateY(-Math.PI / 2);
            stageBoxMesh.rotateX(-Math.PI / 2);
            stageBoxMesh.position.y = y + 15;
            stageBoxMesh.name = String(i + 1);

            let cloneStageBoxMesh = stageBoxMesh.clone();
            arrayStageBox.push(cloneStageBoxMesh);

            scene.add(cloneStageBoxMesh);
        }

        y += 30;
        modelRoof.position.y = y;
        arrayStage.push(modelRoof.children);
        scene.add(modelRoof);

        camera.lookAt(modelRoof.position);
    }, (err) => {
        console.error(`code: ${err.target.status} message: ${err.target.statusText} url: ${err.target.responseURL}`);
    }).catch(err => {
        console.log(err);
    });

    Promise.all([loadModelOfObjMtl("elevator"), loadModelOfObjMtl("door")]).then(results => {
        let modelElevator = results[0];
        let modelDoor = results[1];

        modelDoor.position.z = 2;

        elevator = modelElevator;
        elevatorDoor = modelDoor;
        scene.add(elevator);
        scene.add(elevatorDoor);
    }, (err) => {
        console.error(`code: ${err.target.status} message: ${err.target.statusText} url: ${err.target.responseURL}`);
    }).catch(err => {
        console.log(err);
    });

    renderer = new THREE.WebGLRenderer({
        antialias: false
    });
    renderer.setClearColor(scene.fog.color);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    container = document.getElementById('container');
    container.appendChild(renderer.domElement);

    controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.25;
    controls.rotateSpeed = 0.5;
    controls.enableZoom = true;

    initStats();
    setupGui();

    window.addEventListener('resize', onWindowResize, false);

    render();
}

function loadModelOfObjMtl(name) {
    return new Promise((resolve, reject) => {
        let mtlLoader = new THREE.MTLLoader();
        mtlLoader.setBaseUrl('/');
        mtlLoader.setPath('/model/');
        mtlLoader.load(name + '.mtl', (materials) => {
            materials.preload();
            let objLoader = new THREE.OBJLoader();
            objLoader.setMaterials(materials);
            objLoader.setPath('/model/');
            objLoader.load(name + '.obj', (object) => {
                resolve(object);
            }, (xhr) => {
                if (xhr.lengthComputable) {
                    let percentComplete = xhr.loaded / xhr.total * 100;
                    console.log(Math.round(percentComplete, 2) + '% downloaded obj');
                }
            }, (xhr) => {
                reject(xhr);
            });
        }, (xhr) => {
            if (xhr.lengthComputable) {
                let percentComplete = xhr.loaded / xhr.total * 100;
                console.log(Math.round(percentComplete, 2) + '% downloaded mtl');
            }
        }, (xhr) => {
            reject(xhr);
        });
    });
}

function loadFont(nameFont) {
    return new Promise((resolve, reject) => {
        let loader = new THREE.FontLoader();
        loader.load('fonts/' + String(nameFont), (response) => {
            resolve(response);
        }, (xhr) => {
            if (xhr.lengthComputable) {
                let percentComplete = xhr.loaded / xhr.total * 100;
                console.log(Math.round(percentComplete, 2) + '% downloaded font');
            }
        }, (xhr) => {
            reject(xhr);
        });
    });
}

function setupGui() {
    var gui = new dat.GUI();

    var inElevator = gui.addFolder('inElevator');
    inElevator.add(dataController, "selectInnerStage").min(1).max(Number(countStage)).step(1);
    inElevator.add(dataController, "sendInnerStage");
    inElevator.open();

    var outOfElevator = gui.addFolder('outOfElevator');
    outOfElevator.add(dataController, "selectOuterStage").min(1).max(Number(countStage)).step(1);
    outOfElevator.add(dataController, "sendOuterStage");
    outOfElevator.open();

    gui.add(dataController, "rentgen").onFinishChange(value => {
        if (true === value) {
            arrayStage.forEach(elem => {
                elem.forEach(child => {
                    child.material.depthWrite = false;
                    child.material.transparent = true;
                    child.material.opacity = 0.1;
                });
            })
        } else {
            arrayStage.forEach(elem => {
                elem.forEach(child => {
                    child.material.depthWrite = true;
                    child.material.transparent = false;
                    child.material.opacity = 1;
                });
            })
        }
    })
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    render();
}

function initStats() {
    stats = new Stats();
    stats.setMode(0);

    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '0px';
    stats.domElement.style.top = '0px';

    container.appendChild(stats.domElement);
}

function onMouseClick(event) {
    // calculate mouse position in normalized device coordinates
    // (-1 to +1) for both components
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

    raycaster.setFromCamera(mouse, camera);

    let intersects = raycaster.intersectObjects(scene.children);

    if (intersects.length > 0) {
        INTERSECTED = intersects[0].object;
        if (INTERSECTED.geometry instanceof THREE.TextGeometry) {
            socket.emit("select_inner_stage", Number(INTERSECTED.name));
            INTERSECTED.material.color.set(0xff0000);
        } else if (INTERSECTED.geometry instanceof THREE.BoxGeometry) {
            socket.emit("select_outer_stage", Number(INTERSECTED.name));
            INTERSECTED.material.opacity = 0.5;
        }
    }
}

function animate() {
    if (elevator && elevatorDoor) {
        elevator.position.y = Number(state.y);
        elevatorDoor.position.y = Number(state.y);

        if (state.nextStage) {
            arrayTextNumber[Number(state.currentStage) - 1].material.color.setHex(0xabdf17)
            arrayTextNumber[Number(state.nextStage) - 1].material.color.setHex(0xff0000)
        }

        for (let value of state.innerSet) {
            if (innerSet.has(value)) {
                innerSet.delete(value);
            } else {
                arrayTextNumber[value - 1].material.color.set(0xff0000);
            }
        }
        for (let value of innerSet) {
            arrayTextNumber[value - 1].material.color.set(0xabdf17);
            innerSet.delete(value);
        }

        for (let value of state.outerSet) {
            if (outerSet.has(value)) {
                outerSet.delete(value);
            } else {
                arrayStageBox[value - 1].material.opacity = 0.5;
            }
        }
        for (let value of outerSet) {
            arrayStageBox[value - 1].material.opacity = 0;
            outerSet.delete(value);
        }

        for (let value of state.innerSet) {
            innerSet.add(value);
        }
        for (let value of state.outerSet) {
            outerSet.add(value);
        }

        elevatorDoor.position.x = Number(state.x);
        arrayDoor[Number(state.currentStage) - 1].position.x = Number(state.x);

    }
    requestAnimationFrame(animate);
    render();
}

function render() {
    stats.update();
    controls.update();
    renderer.render(scene, camera);
}

window.addEventListener('click', onMouseClick, false);
