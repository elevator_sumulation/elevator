const gulp = require('gulp');
const livereload = require('gulp-livereload');

gulp.task('elevator', function() {
    gulp.src('./public/elevator.js')
        .pipe(livereload());
});

gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(['./public/elevator.js'], ['elevator']);
});

gulp.task('default', ['watch']);
