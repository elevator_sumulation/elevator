"use strict";

const restify = require('restify');
const socketio = require('socket.io');
const server = restify.createServer();
const _ = require('lodash');
const io = socketio.listen(server.server);

const Engine = require('./engine/engine');

server.get('/', restify.serveStatic({
    directory: './public',
    file: 'index.html'
}));
server.get(/.*/, restify.serveStatic({
    directory: './public'
}))

server.listen(process.env.PORT || '3000', function() {
    console.log('%s listening at %s', server.name, server.url);
});

let countStage = 0;
var state = {
    x: 0,
    y: 0,
    innerSet: [],
    outerSet: [],
    currentStage: 1
};

var userTotal = 0;
var intervalId;

const engine = new Engine();

engine.start();

io.on('connection', function(socket) {
    console.log(countStage);
    if (0 === countStage) {
        socket.emit('get_countStage', 0)
    } else {
        socket.emit('get_countStage', countStage)
    }

    socket.on('set_countStage', function(data) {
        console.log(data);
        countStage = data;
    })

    userTotal++;
    console.log("connect");
    if (!intervalId) {
        intervalId = setInterval(() => {
            state.x = engine.doorPosition;
            state.y = engine.position;
            state.innerSet = _.toArray(engine.stateMachine.innerButtons);
            state.outerSet = _.toArray(engine.stateMachine.outerButtons);
            state.currentStage = engine.stateMachine.currentFloor;
            socket.emit('state', state);
            socket.broadcast.emit('state', state);
        }, 100);

    }
    socket.on('disconnect', function() {
        userTotal--;
        if (userTotal == 0) {
            clearInterval(intervalId);
            intervalId = undefined;
        }
        console.log("disconnect");
    });

    socket.on('select_inner_stage', (data) => {
        const stage = parseInt(data, 10);
        engine.onInnerClicked(stage);
    });

    socket.on('select_outer_stage', (data) => {
        const stage = parseInt(data, 10);
        engine.onOuterClicked(stage);
    });
});
